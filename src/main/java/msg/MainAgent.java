/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import bean.ZoneBean;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import util.SystemMessage;
import util.Xlslib;

/**
 * Zone -> MainAgent ->
 *
 * @author 001343
 */
public class MainAgent extends Agent {

    List<AID> AIDList = new ArrayList<AID>();
    List<AID> TempAIDList = new ArrayList<AID>();
    public static int i = 0;
    public static AID sender = null; // init sender
    public String MESSAGE = "";

    List<ZoneBean> zoneList = new ArrayList<ZoneBean>();

    @Override
    protected void setup() {
        System.out.println("setup...........");
		
		String test = null;
		test.equals("aaa");

        System.out.println("====agentlist====");
        AMSAgentDescription[] agents = null;
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults(new Long(-1));
            agents = AMSService.search(this, new AMSAgentDescription(), c);
        } catch (Exception e) {
        }
        for (int i = 0; i < agents.length; i++) {
            AID agentID = agents[i].getName();
            //add agent list
            System.out.println(agentID.getLocalName());
            if (agentID.getLocalName().contains("Z")) {
                AIDList.add(agentID);
                TempAIDList.add(agentID);
            }
        }
        System.out.println("Agent List Size:" + AIDList.size());
        System.out.println("====/agentlist====");

        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                //send a message to the other agent
//                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
//                msg.setContent("send");
//                msg.addReceiver(new AID("second", AID.ISLOCALNAME));
//                send(msg);
            }
        });

        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {

                String VCO = "";//F1

                System.out.println("============main-agent===========");
                //Receive the other agent message.
                ACLMessage msg = receive();
                System.out.println("msg:" + msg);

                if (msg != null) {
                    System.out.println("receiveMsg:" + msg);

                    try {
                        //validate the init req message
                        String[] split = msg.getContent().split(":");
                        System.out.println("receiveMsg size:" + split.length);
                        if (split.length == 2) {
                            System.out.println("init msg found");
                            MESSAGE = split[0] + ":" + split[1];
                            System.out.println("MESSAGE:-" + MESSAGE);

                            //read the file directory 
                             //get the live-agent list DESC
                            sender = msg.getSender();
                            System.out.println("--live agent list size:"+AIDList.size());
                            for (Iterator<AID> iterator = AIDList.iterator(); iterator.hasNext();) {
                                AID next = iterator.next();
                                if (!next.getLocalName().equalsIgnoreCase(sender.getLocalName())) {
                                    System.out.println(next.getLocalName());
                                    //read file directory get the file uploaded by the agent and get the balance amount related to CODE
                                    System.out.println("Ready:" + next.getLocalName() + " - " + split[0]);
//                                    String tmp = "F:\\ravinath_D\\UB_Project\\VASGO\\VasgoWebApp\\files\\" + next.getLocalName() + ".xls";
                                    String tmp =  SystemMessage.getMessage("filePath") + next.getLocalName() + ".xls";
                                    System.out.println("search:" + tmp);
                                    BigDecimal codeBalance = Xlslib.getCodeBalance(tmp, split[0]); //126-2-3-0-1506
                                    System.out.println("Balance:" + codeBalance);

                                    ZoneBean zb = new ZoneBean();
                                    zb.setAid(next);
                                    zb.setBalance(codeBalance);
                                    zb.setZoneName(next.getLocalName());
                                    
                                    //remove duplicate
                                    boolean flag = true;
                                    for (Iterator<ZoneBean> iterator1 = zoneList.iterator(); iterator1.hasNext();) {
                                        ZoneBean next1 = iterator1.next();
                                        if(next1.getAid().equals(next)){
                                        flag = false;
                                        }
                                    }
                                    
                                    if(flag)
                                    zoneList.add(zb);
                                }
//                                AIDList.remove(next);
                            }

                            //--sort into balance decending order  
                            Collections.sort(zoneList, new Comparator<ZoneBean>() {
                                @Override
                                public int compare(ZoneBean s1, ZoneBean s2) {
                                    return s1.getBalance().compareTo(s2.getBalance());
                                    // return Integer.valueOf(p1.getAge()).compareTo(p2.getAge());
                                }
                            });
                            Collections.reverse(zoneList);
                            //collect sorted AID into AIDList
                            AIDList.removeAll(TempAIDList);
                            for(ZoneBean a:zoneList){
                                AIDList.add(a.getAid());
                            }
                            //--/sort into balance decending order  
                            
                            
                            

                            System.out.println("--/live agent list");
                            //collect the the agent list sort according to the balace amount accending 
                            //{Z1,Z2,Z3}
                            System.out.println("zoneListSize:" + zoneList.size());
                            sender = null;

                            msg.setContent("1000:" + MESSAGE);
                        } else if (split.length == 3 || split.length == 4) {

                        } else {
                            System.err.println("FIX invalid input");
                            // JOptionPane.showMessageDialog(null, "Invalid Message Format found, please follow this CODE:AMOUNT ", "" ,JOptionPane.ERROR_MESSAGE);
                        }
                    } catch (Exception e) {
                        System.err.println("invalid input");
                        JOptionPane.showMessageDialog(null, "Invalid Message Format", "", JOptionPane.ERROR_MESSAGE);

                    }

                    //---CODE:1000 Request message from zone->main----//
                    //str1.toLowerCase().contains(str2.toLowerCase())
                    if (msg.getContent().contains("1000:")) { //request message from zone
                        System.out.println("1000");
                        sender = msg.getSender();

                        //remove sender from AIDList
                        AIDList.remove(sender);

                        addBehaviour(new OneShotBehaviour() {
                            @Override
                            public void action() {
                                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                                msg.setContent("1001:" + MESSAGE);//1001
//                                msg.addReceiver(new AID("Z1", AID.ISLOCALNAME));
                                msg.addReceiver(AIDList.get(i));
                                i++;//sending to first agent
                                send(msg);
                            }
                        });
                    }
                    ////-CODE:1000 Request message from zone->main----//

                    if (msg.getContent().equalsIgnoreCase("3001")) { // appreoved response from Zone-Agent
                        System.out.println("3001");
                        addBehaviour(new OneShotBehaviour() {
                            @Override
                            public void action() {
                                System.out.println("OneShotBehaviour to agent");
                                //send a message to the other agent
                                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                                msg.setContent("2002:" + MESSAGE + ":APROVED");
//                                msg.addReceiver(new AID("Z1", AID.ISLOCALNAME));
                                i = 0;//reset index
                                msg.addReceiver(sender);
                                send(msg);
                            }
                        });
                    }

                    if (msg.getContent().equalsIgnoreCase("3002")) { // reject response from Zone-Agent
                        System.out.println("3002");

                        if (i <= AIDList.size() - 1) {
                            AID aid = AIDList.get(i);
                            System.out.println("next AID:" + aid);
                            addBehaviour(new OneShotBehaviour() {
                                @Override
                                public void action() {
                                    System.out.println("OneShotBehaviour to agent");
                                    //send a message to the other agent
                                    ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                                    msg.setContent("1001:" + MESSAGE);
                                    //get next agent 
//                              msg.addReceiver(new AID("Z1", AID.ISLOCALNAME));
                                   // msg.addReceiver(aid);
                                    send(msg);
                                }
                            });
                            i++;
                        } else {
                            System.err.println("not approved by any agent");
                            i = 0;//reset index
                        }
                        
                    } // reject response from Zone-Agent

                    /*
                        System.out.println("-----Message Send to Agent-------");
                        //send message to Agent  
                        ACLMessage sendmsg = new ACLMessage(ACLMessage.INFORM);
                        sendmsg.setContent("1001");
                        sendmsg.addReceiver(new AID("Z2", AID.ISLOCALNAME));
                        send(sendmsg);
                        System.out.println("-----/Message Send to Agent-------\n\n\n\n\n");
                     */
                    //String senderName = myAgent.getLocalName();
                    //System.out.println("reqAgent:"+senderName);
//                    ACLMessage reply = msg.createReply();
//                    reply.setPerformative(ACLMessage.INFORM);
//                    reply.setContent(replyMessage);
//                    send(reply);
//                    JOptionPane.showMessageDialog(null, "Message receive Back:" + msg.getContent());
//                    //https://www.iro.umontreal.ca/~vaucher/Agents/Jade/primer4.html
//                    System.out.println(" - " + myAgent.getLocalName() + " <- " + msg.getContent());
//                    // block(); 
                    System.out.println("============/main-agent===========");
                } else {
                    block();
                }
            }
        });

    }
}

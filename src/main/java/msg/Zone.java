/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msg;

import bean.MessageBean;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import javax.swing.JOptionPane;
import util.VESGOUtil;

/**
 *
 * @author 001343
 */
public class Zone extends Agent {

    public String MESSAGE = "";

    @Override
    protected void setup() {
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                //Receive the other agent message.
                ACLMessage msg = receive();
                String replyMessage = "";
                if (msg != null) {
                    System.out.println("============zone-agent===========");

                    //extract the message
                    System.out.println("msgContent:" + msg.getContent());
                    MessageBean msgBean = new VESGOUtil().getMessage(msg.getContent());
                    
//                    if (msg.getContent().equalsIgnoreCase("2002")) { //final response message from Main-Agent
                    if (msgBean.getVco().equalsIgnoreCase("2002")) { //final response message from Main-Agent
                        System.out.println("2002");
                        JOptionPane.showMessageDialog(null, "Message receive:" + msgBean.getCode() +" - Amount :Rs "+ msgBean.getAmount() +" - "+ msgBean.getMsg(), "zone:" + myAgent.getLocalName(), JOptionPane.INFORMATION_MESSAGE);
                    }

//                    if (msg.getContent().equalsIgnoreCase("1001")) { //requet message from Main-Agent
                    if (msgBean.getVco().equalsIgnoreCase("1001")) { //requet message from Main-Agent
                        System.out.println("1001");
                        int dialogButton = JOptionPane.YES_NO_OPTION;
                        int showConfirmDialog = JOptionPane.showConfirmDialog(null, msgBean.getCode() +" - "+ msgBean.getAmount() , "zone:" + myAgent.getLocalName(), dialogButton);
                        System.out.println("showConfirmDialog:" + showConfirmDialog);

                        ACLMessage reply = msg.createReply();
                        reply.setPerformative(ACLMessage.INFORM);

                        if (showConfirmDialog == 0) { // Apreoved
                            reply.setContent("3001");
                        } else if (showConfirmDialog == 1) { //Rejected 
                            reply.setContent("3002");
                        }

//                        System.out.println("replyMessageFull:" + msg);
//                        //JOptionPane.showMessageDialog(null, "Message receive:" + msg.getContent());
//                        //https://www.iro.umontreal.ca/~vaucher/Agents/Jade/primer4.html
//                        System.out.println(" - " + myAgent.getLocalName() + " <-- " + msg.getContent());
//                        // block(); 
                        send(reply);
                    }

                    System.out.println("============/zone-agent===========");
                } else {
                    block();
                }
            }
        });
    }
}

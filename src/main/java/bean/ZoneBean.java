/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import jade.core.AID;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author 001343
 */
public class ZoneBean {

    private AID aid;
    private String zoneName;
    private String code;
    private BigDecimal balance;

    
    public ZoneBean(BigDecimal balance) {
        this.balance = balance;
    }

    public ZoneBean() {
    }

    public AID getAid() {
        return aid;
    }

    public void setAid(AID aid) {
        this.aid = aid;
    }
    
    

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public static void main(String[] args) {
        List<ZoneBean> studList = new ArrayList<ZoneBean>();

        studList.add(new ZoneBean(new BigDecimal("150")));
        studList.add(new ZoneBean(new BigDecimal("100")));
        studList.add(new ZoneBean(new BigDecimal("1175")));
        
        Collections.sort(studList, new Comparator<ZoneBean>() {
            @Override
            public int compare(ZoneBean s1, ZoneBean s2) {
                return s1.getBalance().compareTo(s2.getBalance());
                // return Integer.valueOf(p1.getAge()).compareTo(p2.getAge());
            }
        });
        Collections.reverse(studList);
        
        for(ZoneBean s:studList){
            System.out.println(s.getBalance());
        }
        
    }

}

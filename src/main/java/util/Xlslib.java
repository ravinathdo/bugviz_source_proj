/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *
 * @author ravi
 */
public class Xlslib {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        final File folder = new File("files");
//        for (final File fileEntry : folder.listFiles()) {
//            if (fileEntry.isDirectory()) {
//                listFilesForFolder(fileEntry);
//            } else {
//                //file found
//                System.out.println(fileEntry.getName());
//                String filePath = "F:\\ravinath_D\\UB_Project\\VASGO\\VasgoWebApp\\files\\"+fileEntry.getName();
//                BigDecimal codeBalance = new Xlslib().getCodeBalance(filePath, "126-2-3-0-1001");
//                System.out.println("balance:" + codeBalance);
//
//            }
//        }
        
        
        
        
                String filePath = "F:\\ravinath_D\\UB_Project\\VASGO\\VasgoWebApp\\files\\Z2.xls";
//                BigDecimal codeBalance = new Xlslib().getCodeBalance(filePath, "126-2-3-0-1001");
                BigDecimal codeBalance = new Xlslib().getCodeBalance(filePath, "126-2-6-5-2104");
                System.out.println("balance:" + codeBalance);
                
                
        BigDecimal bigDecimal = new BigDecimal("-12812000");
        System.out.println(":"+bigDecimal);
        

    }

    public static void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                System.out.println(fileEntry.getName());
            }
        }
    }

    public static BigDecimal getCodeBalance(String filename, String code) {
        BigDecimal bigDecimal = new BigDecimal("0");
        try {
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filename));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;

            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            int cols = 0; // No of columns
            int tmp = 0;

            // This trick ensures that we get the data properly even if it doesn't start from first few rows
            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) {
                        cols = tmp;
                    }
                }
            }

            for (int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                boolean flag = false;
                if (row != null) {
                    for (int c = 0; c < cols; c++) {
                        cell = row.getCell((short) c);
                        if (cell != null) {
                            // Your code here
                            if (code.equalsIgnoreCase(cell.toString())) {
                                System.out.println("found code " + r + " " + c);
                                flag = true;
                            }

                            if (flag) {
                                if (c == 7) {
                                    try {
                                        BigDecimal bigDecimal1 = new BigDecimal(cell.toString());
                                        System.out.println("Balance:" + bigDecimal1);
                                        return bigDecimal1;
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        }
                    }//for
                    //clear row
                }
                flag = false;
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return bigDecimal;
    }

}

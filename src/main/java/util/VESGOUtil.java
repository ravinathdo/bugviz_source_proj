/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import bean.MessageBean;

/**
 *
 * @author 001343
 */
public class VESGOUtil {

    public MessageBean getMessage(String msg) {
        MessageBean m = new MessageBean();
        //1000:255555:3366555555
        String[] split = msg.split(":");
        System.out.println("size:"+split.length);
        m.setVco(split[0]);
        m.setCode(split[1]);
        if (split.length >= 3) {
            m.setAmount(split[2]);
        }
        if (split.length >= 4) {
            m.setMsg(split[3]);
        }
        return m;
    }
}

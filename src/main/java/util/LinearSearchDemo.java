/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author 001343
 */
class LinearSearchDemo {

    public static void main(String... args) {
        List<Person> list = Arrays.asList(
                Person.create("Oscar", 20),
                Person.create("Reyes", 30),
                Person.create("Java", 10),
                Person.create("Java", 100)
        );

        List<Person> result = searchIn(list,
                new Matcher<Person>() {
            public boolean matches(Person p) {
                return p.getName().equals("Java");
            }
        });

        System.out.println(result);

        result = searchIn(list,
                new Matcher<Person>() {
            public boolean matches(Person p) {
                return p.getAge() > 16;
            }
        });

        System.out.println(result);

    }

    public static <T> List<T> searchIn(List<T> list, Matcher<T> m) {
        List<T> r = new ArrayList<T>();
        for (T t : list) {
            if (m.matches(t)) {
                r.add(t);
            }
        }
        return r;
    }
}

class Person {

    String name;
    int age;

    String getName() {
        return name;
    }

    int getAge() {
        return age;
    }

    static Person create(String name, int age) {
        Person p = new Person();
        p.name = name;
        p.age = age;
        return p;
    }

    public String toString() {
        return String.format("Person(%s,%s)", name, age);
    }
}

interface Matcher<T> {

    public boolean matches(T t);
}
